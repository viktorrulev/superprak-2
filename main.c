// This is the explicit conjugate gradient method for descrete Puasson problem
// on uniform mesh.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#define THREAD_NUM 1

/*
    rank - process id
    Coords[0], Coords[1] - pos in grid
    n0, n1 - size
    left, right, down, up - neighbour id's
*/
MPI_Comm Grid_Comm;             // this is a handler of a new communicator.
int rank;
int power, p0, p1;              // ProcNum = 2^(power), power splits into sum p0 + p1.
int dims[2];                    // dims[0] = 2^p0, dims[1] = 2^p1 (--> M = dims[0]*dims[1]).
int n0, n1, k0, k1;             // N0 = n0*dims[0] + k0, N1 = n1*dims[1] + k1.
int Coords[2];                  // the process coordinates in the cartesian topology created for mesh.

double *in_left_buffer;
double *in_right_buffer;
double *in_up_buffer;
double *in_down_buffer;
double *out_left_buffer;
double *out_right_buffer;
double *out_up_buffer;
double *out_down_buffer;

// the function returns log_{2}(Number) if it is integer. If not it returns (-1). 
int IsPower(int Number)
{
    unsigned int M;
    int p;
    
    if(Number <= 0)
        return(-1);
        
    M = Number; p = 0;
    while(M % 2 == 0)
    {
        ++p;
        M = M >> 1;
    }
    if((M >> 1) != 0)
        return(-1);
    else
        return(p);
    
}

// This is the splitting procedure of proc. number p. The integer p0
// is calculated such that abs(N0/p0 - N1/(p-p0)) --> min.
int SplitFunction(int N0, int N1, int p)
{
    float n0, n1;
    int p0, i;
    
    n0 = (float) N0; n1 = (float) N1;
    p0 = 0;
    
    for(i = 0; i < p; i++)
        if(n0 > n1)
        {
            n0 = n0 / 2.0;
            ++p0;
        }
        else
            n1 = n1 / 2.0;
    
    return(p0);
}

// Domain size.
const double A = 3.0;
const double B = 3.0;

int NX, NY;	   // the number of internal points on axes (ox) and (oy).
double hx, hy; // mesh steps on (0x) and (0y) axes

#define Step 10
#define Epsilon 1e-4

#define Max(A,B) ((A)>(B)?(A):(B))
#define R2(x,y) ((x)*(x)+(y)*(y))
#define Cube(x) ((x)*(x)*(x))
#define x(i) ((i)*hx)
#define y(j) ((j)*hy)

#define LeftPart(P,i,j)\
((-(P[(n0 + 2)*(j)+i+1]-P[(n0 + 2)*(j)+i])/hx+(P[(n0 + 2)*(j)+i]-P[(n0 + 2)*(j)+i-1])/hx)/hx+\
 (-(P[(n0 + 2)*(j+1)+i]-P[(n0 + 2)*(j)+i])/hy+(P[(n0 + 2)*(j)+i]-P[(n0 + 2)*(j-1)+i])/hy)/hy)

void mpi_exchange(double *SolVect, int counter);

void local_to_cartesian(int local_i, int local_j, int *cartesian_i, int *cartesian_j)
{
    if (Coords[0] < k0)
        *cartesian_i = Coords[0] * n0 + local_i;
    else
        *cartesian_i = Coords[0] * n0 + k0 + local_i;
    
    if (Coords[1] < k1)
        *cartesian_j = (Coords[1] + 1) * n1 - local_j + 1; 
    else
        *cartesian_j = (Coords[1] + 1) * n1 + k1 - local_j + 1; 
}

void local_to_global(int local_i, int local_j, int *global_i, int *global_j)
{
    if (Coords[0] < k0)
        *global_i = Coords[0] * n0 + local_i; 
    else
        *global_i = Coords[0] * n0 + k0 + local_i; 
    
    if (Coords[1] < k1)
        *global_j = (NY + 2) - ((Coords[1] + 1) * n1 - local_j + 1) - 1;
    else
        *global_j = (NY + 2) - ((Coords[1] + 1) * n1 + k1 - local_j + 1) - 1;
}

double Solution(double x, double y)
{
    return log(1.0 + x * y);
}

double BoundaryValue(double x, double y)
{
    return Solution(x, y);
}

void RightPart(double *rhs)
{
    int i, j;

    for (j = 0; j < n1 + 2; j++)
        for (i = 0; i < n0 + 2; i++)
        {
            int cartesian_i, cartesian_j;
            local_to_cartesian(i, j, &cartesian_i, &cartesian_j);
            
            rhs[(n0 + 2) * j + i] = R2(x(cartesian_i), y(cartesian_j)) / ( (1.0 + x(cartesian_i) * y(cartesian_j)) * (1.0 + x(cartesian_i) * y(cartesian_j)) );
        }
}

void steep_next_approximation(double *ResVect, double *SolVect, double *RHS_Vect, double *BasisVect, double *sp)
{
    int i, j;
    double global_tau, local_sp, global_sp;
    
    // The residual vector r(k) = Ax(k) - f is calculating ...
	#pragma omp parallel for collapse(2) default(none) private(i, j, n0, n1, SolVect, RHS_Vect) shared(ResVect)
	for (j = 1; j < n1 + 1; j++)
		for (i = 1; i < n0 + 1; i++)
            ResVect[(n0 + 2) * j + i] = LeftPart(SolVect, i, j) - RHS_Vect[(n0 + 2) * j + i];

    // The value of product (r(k), r(k)) is calculating ...
	local_sp = 0.0;
	#pragma omp parallel for collapse(2) reduction(+: local_sp)
	for (j = 1; j < n1 + 1; j++)
		for (i = 1; i < n0 + 1; i++)
			local_sp += ResVect[(n0 + 2) * j + i] * ResVect[(n0 + 2) * j + i] * hx * hy;
    MPI_Allreduce(&local_sp, &global_sp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    global_tau = global_sp;
    
    mpi_exchange(ResVect, 0);

    // The value of product sp = (Ar(k), r(k)) is calculating ...
	local_sp = 0.0;
	#pragma omp parallel for collapse(2) reduction(+: local_sp)
	for (j = 1; j < n1 + 1; j++)
		for (i = 1; i < n0 + 1; i++)
			local_sp += LeftPart(ResVect, i, j) * ResVect[(n0 + 2) * j + i] * hx * hy;
    MPI_Allreduce(&local_sp, &global_sp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	global_tau = global_tau / global_sp;

    // The x(k+1) is calculating ...
	#pragma omp parallel for collapse(2) default(none) private(i, j, n0, n1, ResVect) shared(SolVect)
	for (j = 1; j < n1 + 1; j++)
		for (i = 1; i < n0 + 1; i++)
			SolVect[(n0 + 2) * j + i] = SolVect[(n0 + 2) * j + i] - global_tau * ResVect[(n0 + 2) * j + i];
    
    *sp = global_sp;
}

double CG_next_approximation(double *ResVect, double *SolVect, double *RHS_Vect, double *BasisVect, double *sp, int counter)
{
    int i, j;
    double local_tau, global_tau, local_alpha, global_alpha, new_local_sp, new_global_sp, max_difference, difference;

    // The residual vector r(k) is calculating ...
    #pragma omp parallel for collapse(2) default(none) private(i, j, n0, n1, SolVect, RHS_Vect) shared(ResVect)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
            ResVect[(n0 + 2) * j + i] = LeftPart(SolVect, i, j) - RHS_Vect[(n0 + 2) * j + i];
    
    mpi_exchange(ResVect, counter);

    // The value of product (Ar(k),g(k-1)) is calculating ...
    local_alpha = 0.0;
    #pragma omp parallel for collapse(2) reduction(+: local_alpha)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
            local_alpha += LeftPart(ResVect, i, j) * BasisVect[(n0 + 2) * j + i] * hx * hy;
    MPI_Allreduce(&local_alpha, &global_alpha, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    global_alpha = global_alpha / *sp;

    // The new basis vector g(k) is being calculated ...
    #pragma omp parallel for collapse(2) default(none) private(i, j, n0, n1, SolVect, RHS_Vect) shared(ResVect)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
            BasisVect[(n0 + 2) * j + i] = ResVect[(n0 + 2) * j + i] - global_alpha * BasisVect[(n0 + 2) * j + i];

    // The value of product (r(k),g(k)) is being calculated ...
    local_tau = 0.0;
    #pragma omp parallel for collapse(2) reduction(+: local_tau)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
            local_tau += ResVect[(n0 + 2) * j + i] * BasisVect[(n0 + 2) * j + i] * hx * hy;
    MPI_Allreduce(&local_tau, &global_tau, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    mpi_exchange(BasisVect, counter);

    // The value of product sp = (Ag(k),g(k)) is being calculated ...
    new_local_sp = 0.0;
    #pragma omp parallel for collapse(2) reduction(+: new_local_sp)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
            new_local_sp += LeftPart(BasisVect, i, j) * BasisVect[(n0 + 2) * j + i] * hx * hy;
    MPI_Allreduce(&new_local_sp, &new_global_sp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    global_tau = global_tau / new_global_sp;
    *sp = new_global_sp;

    // The x(k+1) is being calculated ...
    difference = 0.0;
    #pragma omp parallel for collapse(2) default(none) private(i, j, n0, n1, BasisVect) shared(SolVect)
    for (j = 1; j < n1 + 1; j++)
        for (i = 1; i < n0 + 1; i++)
        {
            double NewValue = SolVect[(n0 + 2) * j + i] - global_tau * BasisVect[(n0 + 2) * j + i];
            difference = Max(difference, fabs(NewValue - SolVect[(n0 + 2) * j + i]));
            SolVect[(n0 + 2) * j + i] = NewValue;
        }
    MPI_Allreduce(&difference, &max_difference, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    return max_difference;
}

void mpi_exchange(double *SolVect, int counter)
{
    int i;
    
    for (i = 1; i < n1 + 1; i++)
    {
        out_left_buffer[i - 1] =  SolVect[(n0 + 2) * i + 1];
        out_right_buffer[i - 1] = SolVect[(n0 + 2) * i + n0];
    }
    
    for (i = 1; i < n0 + 1; i++)
    {
        out_up_buffer[i - 1] =   SolVect[(n0 + 2) * 1 + i];
        out_down_buffer[i - 1] = SolVect[(n0 + 2) * n1 + i];
    }
    
    int rank, left, right, up, down;
    MPI_Request req_left[2], req_right[2], req_up[2], req_down[2];
    MPI_Status st_left[2], st_right[2], st_up[2], st_down[2];
    
    MPI_Cart_shift(Grid_Comm, 0, 1, &left, &right);
    MPI_Cart_shift(Grid_Comm, 1, 1, &down, &up);
    
    if (left >= 0)
    {
        MPI_Isend(out_left_buffer, n1, MPI_DOUBLE, left, counter * 4 + 1, MPI_COMM_WORLD, &req_left[0]);
        MPI_Irecv(in_left_buffer,  n1, MPI_DOUBLE, left, counter * 4 + 0, MPI_COMM_WORLD, &req_left[1]);
    }
    
    if (right >= 0)
    {
        MPI_Isend(out_right_buffer, n1, MPI_DOUBLE, right, counter * 4 + 0, MPI_COMM_WORLD, &req_right[0]);
        MPI_Irecv(in_right_buffer,  n1, MPI_DOUBLE, right, counter * 4 + 1, MPI_COMM_WORLD, &req_right[1]);
    }
    
    if (up >= 0)
    {
        MPI_Isend(out_up_buffer, n0, MPI_DOUBLE, up, counter * 4 + 3, MPI_COMM_WORLD, &req_up[0]);
        MPI_Irecv(in_up_buffer,  n0, MPI_DOUBLE, up, counter * 4 + 2, MPI_COMM_WORLD, &req_up[1]);
    }
    
    if (down >= 0)
    {
        MPI_Isend(out_down_buffer, n0, MPI_DOUBLE, down, counter * 4 + 2, MPI_COMM_WORLD, &req_down[0]);
        MPI_Irecv(in_down_buffer,  n0, MPI_DOUBLE, down, counter * 4 + 3, MPI_COMM_WORLD, &req_down[1]);
    }
    
    if (left >= 0)
        MPI_Waitall(2, req_left, st_left);
    
    if (right >= 0)
        MPI_Waitall(2, req_right, st_right);
    
    if (up >= 0)
        MPI_Waitall(2, req_up, st_up);
    
    if (down >= 0)
        MPI_Waitall(2, req_down, st_down);
    
    for (i = 1; i < n1 + 1; i++)
    {
        if (left >= 0)
            SolVect[(n0 + 2) * i + 0] =  in_left_buffer[i - 1];
        
        if (right >= 0)
            SolVect[(n0 + 2) * i + (n0 + 1)] = in_right_buffer[i - 1];
    }
    
    for (i = 1; i < n0 + 1; i++)
    {
        if (up >= 0)
            SolVect[(n0 + 2) * 0 + i] =  in_up_buffer[i - 1];
        
        if (down >= 0)
            SolVect[(n0 + 2) * (n1 + 1) + i] = in_down_buffer[i - 1];
    }
}

void print_full_matrix(double *full_matrix)
{
    int i, j;
    
    for (j = 0; j < NX + 2; j++)
    {
        for (i = 0; i < NY + 2; i++)
            printf("%8.4lf ", full_matrix[(NX + 2) * j + i]);
        
        printf("\n");
    }
    
    printf("\n");
}

void print_sol_matrix(double *full_matrix)
{
    int i, j;
    
    for (j = 0; j < n0 + 2; j++)
    {
        for (i = 0; i < n1 + 2; i++)
            printf("%8.4lf ", full_matrix[(n0 + 2) * j + i]);
        
        printf("\n");
    }
    
    printf("\n");
}

void gather_matrix(double *matrix)
{
    int pn, i, j;
    int ProcNum;
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank == 0)
    {
        double *full_matrix = (double *)calloc((NX + 2) * (NY + 2), sizeof(double));
        
        for (i = 0; i < NX + 2; i++)
        {
            full_matrix[i * (NX + 2) + 0] = BoundaryValue(0.0, y((NY + 2) - i - 1));
            full_matrix[i * (NX + 2) + (NY + 1)] = BoundaryValue(A, y((NY + 2) - i - 1));
        }

        for (i = 0; i < NY + 2; i++)
        {
            full_matrix[0 * (NX + 2) + i] = BoundaryValue(x(i), B);
            full_matrix[(NY + 1) * (NX + 2) + i] = BoundaryValue(x(i), 0.0);
        }
        
        /// SELF
        for (j = 1; j < n1 + 1; j++)
            for (i = 1; i < n0 + 1; i++)
            {
                int global_i, global_j;
                    
                local_to_global(i, j, &global_i, &global_j);
                
                full_matrix[(NX + 2) * global_j + global_i] = matrix[(n0 + 2) * j + i];
            }
        ///
        
        int pos_size[6], x, y;
        
        for (pn = 1; pn < ProcNum; pn++)
        {
            MPI_Recv(pos_size, 6, MPI_INT, pn, pn * 2 + 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
            Coords[0] = pos_size[0];
            Coords[1] = pos_size[1];
            n0 = pos_size[2];
            n1 = pos_size[3];
            k0 = pos_size[4];
            k1 = pos_size[5];
            
            double *buffer = (double *)calloc((n0 + 2) * (n1 + 2), sizeof(double));
            
            MPI_Recv(buffer, (n0 + 2) * (n1 + 2), MPI_DOUBLE, pn, pn * 2 + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
            for (j = 1; j < pos_size[3] + 1; j++)
                for (i = 1; i < pos_size[2] + 1; i++)
                {
                    int global_i, global_j;
                    
                    local_to_global(i, j, &global_i, &global_j);
                    
                    full_matrix[(NX + 2) * global_j + global_i] = buffer[(n0+ 2) * j + i];
                }
            
            
            free(buffer);
        }
        
        //print_full_matrix(full_matrix);
        
        free(full_matrix);
    }
    else
    {
        int pos_size[6];
        pos_size[0] = Coords[0];
        pos_size[1] = Coords[1];
        pos_size[2] = n0;
        pos_size[3] = n1;
        pos_size[4] = k0;
        pos_size[5] = k1;
        
        MPI_Send(pos_size, 6, MPI_INT, 0, rank * 2 + 0, MPI_COMM_WORLD);
        MPI_Send(matrix, (n0 + 2) * (n1 + 2), MPI_DOUBLE, 0, rank * 2 + 1, MPI_COMM_WORLD);
    }   
}

int main(int argc, char *argv[])
{
	// OpenMP
	omp_set_num_threads(THREAD_NUM);
	
    ////////////////////////////////////////////////////////////////////////////////////////////
    // grid
    if (argc < 3)
    {
        printf("Wrong number of parameters in command line.\nUsage: <ProgName> "\
               "<Nodes number on (0x) axis> <Nodes number on (0y) axis> "\
               "Finishing...\n");

        return(-1);
    }

    NX = atoi(argv[1]) - 2;
    NY = atoi(argv[2]) - 2;
    
    
    hx = A / (NX + 2 - 1);
    hy = B / (NY + 2 - 1);
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MPI stuff
    int ProcNum;//, rank;              // the number of processes and rank in communicator.
    int ndims = 2;                  // the number of a process topology dimensions.
    int periods[2] = {0,0};         // it is used for creating processes topology.
    int left, right, up, down;      // the neighbours of the process.

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    double start_time = MPI_Wtime();
    
    power = IsPower(ProcNum);
    p0 = SplitFunction(NX, NY, power);
    p1 = power - p0;
    dims[0] = (unsigned int) 1 << p0;
    dims[1] = (unsigned int) 1 << p1;
    n0 = NX >> p0;
    n1 = NY >> p1;
    k0 = NX - dims[0] * n0;
    k1 = NY - dims[1] * n1;
    
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, 1, &Grid_Comm);
    MPI_Comm_rank(Grid_Comm, &rank);
    MPI_Cart_coords(Grid_Comm, rank, ndims, Coords);
    
    if(Coords[0] < k0)
        ++n0;
    if(Coords[1] < k1)
        ++n1;
    
    MPI_Cart_shift(Grid_Comm, 0, 1, &left, &right);
    MPI_Cart_shift(Grid_Comm, 1, 1, &down, &up);
   
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MATRIX stuff
	double *SolVect;					    // the solution array
    double *ResVect;					    // the residual array
	double *BasisVect;					    // the vector of A-orthogonal system in CGM
	double *RHS_Vect;					    // the right hand side of Puasson equation
	double sp;

	int i, j;
	char str[127];
	FILE * fp;

	SolVect  = (double *)calloc((n0 + 2) * (n1 + 2), sizeof(double));
	ResVect  = (double *)calloc((n0 + 2) * (n1 + 2), sizeof(double));
	RHS_Vect = (double *)calloc((n0 + 2) * (n1 + 2), sizeof(double));
    
    in_left_buffer = (double *)calloc(n1, sizeof(double));
    in_right_buffer = (double *)calloc(n1, sizeof(double));
    in_up_buffer = (double *)calloc(n0, sizeof(double));
    in_down_buffer = (double *)calloc(n0, sizeof(double));
    out_left_buffer = (double *)calloc(n1, sizeof(double));
    out_right_buffer = (double *)calloc(n1, sizeof(double));
    out_up_buffer = (double *)calloc(n0, sizeof(double));
    out_down_buffer = (double *)calloc(n0, sizeof(double)); 
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    // Initialization of Arrays
    RightPart(RHS_Vect);
    
    int global_i, global_j;
    
    if (left < 0)
    {
        for (i = 0; i < n1 + 2; i++)
        {
            local_to_cartesian(0, i, &global_i, &global_j);
            SolVect[i * (n0 + 2) + 0] = BoundaryValue(x(global_i), y(global_j));
        }
    }
    
    if (right < 0)
    {
        for (i = 0; i < n1 + 2; i++)
        {
            local_to_cartesian(n0 + 1, i, &global_i, &global_j);
            SolVect[i * (n0 + 2) + (n0 + 1)] = BoundaryValue(x(global_i), y(global_j));
        }
    }
    
    if (up < 0)
    {
        for (i = 0; i < n0 + 2; i++)
        {
            local_to_cartesian(i, 0, &global_i, &global_j);
            SolVect[0 * (n0 + 2) + i] = BoundaryValue(x(global_i), y(global_j));
        }
    }
    
    if (down < 0)
    {
        for (i = 0; i < n0 + 2; i++)
        {
            local_to_cartesian(i, n1 + 1, &global_i, &global_j);
            SolVect[(n1 + 1) * (n0 + 2) + i] = BoundaryValue(x(global_i), y(global_j));
        }
    }
    
    /// steep approximation
	steep_next_approximation(ResVect, SolVect, RHS_Vect, BasisVect, &sp);
    
    /// MPI BLOCK
    mpi_exchange(SolVect, 0);
    ///

	BasisVect = ResVect;
    ResVect = (double *)calloc((n0 + 2) * (n1 + 2), sizeof(double));

    /////////////////////////////////////////////////////////////////////
    // CGM iterations begin ...
    
	int counter = 1;
    while (1)
	{
        double difference = CG_next_approximation(ResVect, SolVect, RHS_Vect, BasisVect, &sp, counter);
        
        /// MPI BLOCK
        mpi_exchange(SolVect, counter);
        ///
        
        if (rank == 0)
			printf("step %d, diff = %lf\n", counter, difference);
        
        counter++;
        
        if (difference < Epsilon)
            break;
    }
    
    // the end of CGM iterations.
    /////////////////////////////////////////////////////////////////////
    
    /// MPI BLOCK
    gather_matrix(SolVect);
    ///
    
    free(SolVect);
	free(ResVect);
	free(BasisVect);
	free(RHS_Vect);
    
    free(in_left_buffer);
    free(out_left_buffer);
    free(in_right_buffer);
    free(out_right_buffer);
    free(in_up_buffer);
    free(out_up_buffer);
    free(in_down_buffer);
    free(out_down_buffer);
    
    double elapsed_time = MPI_Wtime() - start_time;
    double max_time;
    
    MPI_Reduce(&elapsed_time, &max_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	
	if (rank == 0)
    {
        printf("Grid size: %dx%d, process number: %d, thread number: %d", NX + 2, NY + 2, ProcNum, THREAD_NUM);   
    }
    
    if (rank == 0)
        printf(" time: %lf\n", max_time);
    
    MPI_Finalize();

	return(0);
}
